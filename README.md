Summary:
This is a interactive chatbot creayed by Himanshu Kandari.
It is based on horror Vampire Story, where user have to chose and write one of the words written in bold capital.
This was based on my previous assignment for Mobile Application Development.
I have used node.js and javascript for this assignment.
You need to have Visual Studio code and node.js installed to run this program successfully and checkc its program.

Steps:
Download the source code.
Download Visual Studio and open the respective project folder in it
Open the terminal to run the project
Check the installed version of nodejs by typing "node --version". if node is not pre-Installed then type "npm install" in the terminal to install nodejs
To run the project in console then type "node IndexConsole.js"

# License
Copyright © Himanshu Kandari - 2020
