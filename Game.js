const GameState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    LIGHT:  Symbol("light"),
    WAIT: Symbol("wait"),
    GIRL: Symbol("girl"),
    BAR: Symbol("bar"),
    INVITE: Symbol("invite")
});

module.exports = class Game{
    constructor(){
        this.stateCur = GameState.WELCOMING;
    }
    
    makeAMove(sInput)
    {
        let sReply = "";
        switch(this.stateCur){
            case GameState.WELCOMING:
                sReply = "You are on vacation and being an adventurous person, you decided to go to a remote area with your best friend. You both arrive at your destination at night, the lights abruptly go out, and you have very little charge on your cell phone. A sudden burst of light grabs your attention. It is the neighbors’ house party, leaving you curious. Would you like to <strong>WAIT</strong> or <strong>GO</strong> to the party?";
                this.stateCur = GameState.LIGHT;
                break;
            case GameState.LIGHT:
                if(sInput.toLowerCase().match("wait")){
                    sReply = "The mobile battery is about to die and there is no one els nearby. The night is getting gloomy and you need help. Will you <strong>GO</strong> to the party or keep <strong>Waiting</strong>?";
                }else{
                    sReply ="You start walking towards the party while your friend is a lookout. As everybody seems to be wearing a masquerade, it seems to be a costume party. A girl dressed in all black, you ask her about the party. She moves closer to you, provocatively whispering in your ear and asking you to join her. Are you going to <strong>FOLLOW</strong> her, or <strong>WALK</strong> back to your friend in the house?";
                    this.stateCur = GameState.GIRL;
                }
                break;
            case GameState.GIRL:
                if(sInput.toLowerCase().match("follow")){
                    sReply = "She walks you to the bar, but you do not wish to drink. Regardless of your decision she orders a drink for you and smirk stating that is not what I really want. The bar tender presents you with a sadistic looking scarlet drink. Icy when you touch the glass. A sip and your feel the burn in your throat. You feel thirstier. Are you going to <strong>ACCEPT</strong> the drink or <strong>REJECT</strong> it and go back to your house?"
                    this.stateCur = GameState.BAR;
                }else{
                    sReply = "You go home and wait hours, but still no light. Your phone battery is about to die and there is no one nearby to help. It is getting gloomier. Do you want to keep <strong>Waiting</strong> or do you want to <strong>GO</strong> to the party?";
                    this.stateCur = GameState.LIGHT;

                }
                break;
            case GameState.BAR:
                if(sInput.toLowerCase().match("reject")){
                    sReply = "You run back home and have a glass of water, but it is useless. You are craving for the drink. The night is getting gloomier. You uncontrollable. You go back to the party. Deja Vu !! You see that girl again. You again <strong>FOLLOW</strong> or <strong>NOT</strong>?";
                    this.stateCur = GameState.GIRL;

                }else{
                    sReply = "You are enjoying your drink. With every sip you keep quenching for more. Suddenly it dawns to you that the liquid feels heavier than any regular drink of alcohol. While ordering another one, a little spill of the drink on your hand makes you realize it is not alcohol, but blood! Horrified you feel like throwing up, when abruptly you get a call from your friend, who is just checking on you. Will you invite her for <strong>HELP</strong> or <strong>FIGHT</strong> them alone?";
                    this.stateCur = GameState.INVITE;
    
                }
                break;
            case GameState.INVITE:
                if(sInput.toLowerCase().match("fight")){
                    sReply = "You turn back and see that the vampires have got your friend. The bartender leers and says that my drink has arrived! Welcome to the world of Vampires. Game Over!! ";
                    this.stateCur = GameState.WELCOMING;
                }else{
                    sReply = "Your friend arrives with Holy water and reverses the spell. You are saved !!";
                    this.stateCur = GameState.WELCOMING;
                }
        }
        return([sReply]);
    }
}